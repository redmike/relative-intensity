import styled from "styled-components";
import {relativeIntensity} from "../helper/definitions"
import {PINK, YELLOW, TURQUOISE, FUCHSIA, DARK, LIGHT} from '../global'

const Container = styled.div`
    &:before {
      content: '${relativeIntensity}';
      display: none;
      position: absolute;
      transform: translate(0, 2rem);
      //background-image: linear-gradient(25deg, violet, blue);
      background-image: linear-gradient(25deg, ${DARK}, ${DARK}, ${DARK}, ${TURQUOISE + '80'}, ${TURQUOISE});
      opacity: 0.8;
      // border: 1px solid ${YELLOW};
      border: 1px solid ${LIGHT};
      padding: 0.5rem 1rem;
      border-radius: 5px;
      max-width: 50vw;
      
    }
  &:hover:before {
    display: block;
  }
  
    
`;

export const Header = () => {
    return (
        <Container>
            <h1>Relative Intensity Calculator</h1>
        </Container>
    )

}
