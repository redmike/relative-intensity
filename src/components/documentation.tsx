import styled from "styled-components";
import {Link} from "react-router-dom";

const Container = styled.div``;
export const Documentation = () => {
    return (
        <Container>
            <Link to="/about">Read about it</Link>
        </Container>
    )
};
// https://en.wikipedia.org/wiki/One-repetition_maximum
