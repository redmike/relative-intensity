export const strain = 'Strain means the weight you lifted, can be in kgs or lbs, the result will be in the same unit.'
export const reps = 'Reps means how many repetitions you did in the target set.'
export const relativeIntensity = 'Relative Intensity is a way to calculate the effort or "intensity" made in a specific movement.'
export const formula = 'Different formulas will have diferent result for the same inputs, stick with one.';
export const result = 'the weight unit doesnt matter, if you put the strain in kgs, results in kgs, if you did it in pounds, result in lbs.';
