import styled from "styled-components";
import {Link} from "react-router-dom";

const Container = styled.div`
  a {
    //text-decoration: none;
    color: inherit;
  }
`
export const About = (dark:any) => {
    return (
        <Container>
            <Link to="/">
                back
            </Link>
            <h1>Relative Intensity Calculator</h1>
            <h2>What solve?</h2>
            <p>Aims to help to the lifter in the calculus of the intensity in every sets.</p>
            <h2>Why use this formula?</h2>
            <p>
                The 1RM is the heaviest weight that one person can lift, trying that often will lead to an injury.
                This method aim to get the intensity of the a ie 3RM to a 1RM.
            </p>
            <p>
                If you backsquat 3 reps @ 200kgs, the absolute intensity of that set was 200kgs, but the relative intensity is more than that.
                This way you can feel more confident when you lift a new weight, if you already reach that intensity as relative intensity.
            </p>
            <h2>Definitions</h2>
            <ul>
                <h3>Formulas</h3>
                <li>Epley</li>
                <li>Brzycki</li>
            </ul>
            <ul>
                <h3>Terms</h3>
                <li>Relative Instensity</li>
                <p>Definition of relative intensity</p>
                <li>Absolute Intensity</li>
                <p>Definition of absolute intensity</p>
            </ul>
        </Container>
    )
}
