import styled from 'styled-components';
import {Header} from '../components/header';
import {Documentation} from '../components/documentation'
import { useState } from 'react';
import {epleyFormula, brzyckiFormula} from '../helper/calculator'
import * as events from "events";
import {PINK, YELLOW, TURQUOISE, FUCHSIA, DARK, LIGHT} from '../global'
import {strain, reps, formula, result} from "../helper/definitions"

const Container = styled.div`
  min-width: 100vh;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  //border: 1px solid ${LIGHT};
  //border-radius: 5px;
  padding: 1rem;
  a {
    //text-decoration: none;
    color: inherit;
  }
  div.boxes {
    display: flex;
  }
  label {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 0.5rem 0.25rem;
    &#formula select {
      text-transform: capitalize;
      font-weight: bold;
      cursor: pointer;
      background: ${DARK};
      color: ${LIGHT};
      border: 1px solid ${DARK};
      padding: 0.25rem;
      border-radius: 5px;
      &:hover {
        border-color: ${LIGHT};
      }
    }
    &#strain,
    &#reps,
    &#formula,
    &#result {
      &:before {
        display: none;
        position: absolute;
        transform: translate(0, 3.5rem);
        background-image: linear-gradient(25deg, ${DARK}, ${DARK}, ${DARK}, ${TURQUOISE + '80'}, ${TURQUOISE});
        opacity: 0.9;
        border: 1px solid ${LIGHT};
        padding: 0.5rem 1rem;
        border-radius: 5px;
        max-width: 50vw;
      }
    }
    &#strain:hover:before {
      display: block;
      content: '${strain}';
    }
    &#reps:hover:before {
      display: block;
      content: '${reps}';
    }
    &#formula:hover:before {
      display: block;
      content: '${formula}';
    }
    &#result:hover:before {
      display: block;
      content: '${result}';
    }
  }
`;
const Input = styled.input`
  width: 4rem;
  height: 2rem;
  text-align: center;
  border-radius: 5px;
  border: 1px solid ${({theme}) => true ? YELLOW : FUCHSIA};
  color: ${({theme}) => true ? LIGHT : DARK};
  background-color: ${({theme}) => true ? DARK : LIGHT};
  &:hover {
    border-color: ${({theme}) => true ? TURQUOISE : YELLOW};
  }
`;
const Result = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  width: 4rem;
  height: 2rem;
  text-align: center;
  border-radius: 5px;
  border: 1px solid ${({theme}) => true ? YELLOW : FUCHSIA};
  color: ${({theme}) => true ? LIGHT : DARK};
  background-color: ${({theme}) => true ? DARK : LIGHT};
  &:hover {
    border-color: ${({theme}) => true ? TURQUOISE : YELLOW};
  }
`;

const Convert = (dark:any) => {
    const [strain, setStrain] = useState(0);
    const [reps, setReps] = useState(0);
    const [formula, setFormula] = useState('epley');
    function calculateIntensity(strain: number, reps: number, formula: string) {
        if (reps === 0) return 0;
        else if (reps === 1) return strain;
        switch (formula) {
            case 'epley': return epleyFormula(strain, reps);
            case 'brzycki': return brzyckiFormula(strain, reps);
        }
    }
    return (
        <Container>
            <Header/>
            <div className="boxes">
                <label id="strain"> <span>Strain</span>
                    <Input placeholder="Strain/Weight" type="text" required value={strain} onChange={(e) => setStrain(+e.target.value)} theme={dark}/>
                </label>
                <label id="reps"> Reps
                    <Input placeholder="Reps" type="text" required value={reps} onChange={(e) => setReps(+e.target.value)} theme={dark}/>
                </label>
            </div>
            <label id="formula"> Formula
                <select onChange={(e) => setFormula(e.target.value)}>
                    <option value="epley">epley</option>
                    <option value="brzycki">brzycki</option>
                </select>
            </label>
            <label id="result">Result
                <Result>{calculateIntensity(strain, reps, formula)}</Result>
            </label>
            <Documentation/>
        </Container>
    )
}

export default Convert
